// ##############################
// Author  : Crazychicken - Tuds
// Name    : Convert Languages
// Version : 1.0.8
// Github  : https://github.com/crazychicken/converthtml
// ##############################
var getText = document.getElementById('inputText');
var subMit = document.getElementById('subMit');
var showText = document.getElementById('showText');
var outputText = document.getElementById('outputText');  // output content
var checkNum = '1234567890';
var alphabet = "abcdefghijklmnopqrstuvwxyz";


// GLOBAL FUNCTION
// Function Find Space
function fn_findSpace(data) {
	maxSpace = 0;
	// Xác định maxloop
	data = data.replace(/\ /g, '\♥');
	data = data.replace(/\♥\♥/gi, '\<\♥\♥');
	data = data.replace(/\♥\<\♥/gi, '\♥\♥');
	function repSpace() {
		if ( data.indexOf('\<\♥\♥') !== -1 ) {
			data = data.replace(/\<\♥\♥/gi, '\<\♥');
			repSpace();
		}
		if ( maxSpace > 500 ) {
			return;
		}
		// console.log(maxSpace);
		return maxSpace++;
	}
	repSpace();
	return maxSpace;
}

// Note CSS + SASS /**/ //
function findNoted(data) {
	// &sbquo; => ♣♦44♠ => ,	
	data = data.replace(/\,/gi, '♣♦44♠') // All ,
	data = data.replace(/\ /g, '\♥');
	data = data.split(' ');
	function replaceNoted() {
		// ♣ => &    ♦ => #    ♠ => ;
		data[i] = data[i].replace(/\@/g, '♣♦n64♠');
		data[i] = data[i].replace(/\:/g, '♣♦n58♠');
		data[i] = data[i].replace(/\;/g, '♣♦n59♠');
		data[i] = data[i].replace(/\./g, '♣♦n46♠');
		data[i] = data[i].replace(/\"/g, '♣♦n34♠');
		data[i] = data[i].replace(/\#/g, '♣♦n35♠');
		data[i] = data[i].replace(/\$/g, '♣♦n36♠');
		data[i] = data[i].replace(/\'/g, '♣♦n39♠');
		data[i] = data[i].replace(/\(/g, '♣♦n40♠');
		data[i] = data[i].replace(/\)/g, '♣♦n41♠');
		data[i] = data[i].replace(/\=/g, '♣♦n61♠');
		data[i] = data[i].replace(/\{/g, '♣♦n123♠');
		data[i] = data[i].replace(/\}/g, '♣♦n125♠');
		data[i] = 's♠note' + data[i] + 'e♠note\n';
		data[i] = data[i].replace('\n\e♠note', 'e♠note');
	}
	for ( var i = 0; i < data.length; i++ ) {
		if ( data[i].indexOf('//') !== -1 ) {
			data[i] = data[i].replace(/\/\//g, '\ \/\/');
		}
	}
	data = data.toString();
	data = data.replace(/\,/g, '');
	data = data.replace(/\n/g, '\n\ ');
	data = data.split(' ');
	for ( var i = 0; i < data.length; i++ ) {
		if ( data[i].indexOf('//') !== -1 ) {
			data[i] = data[i].replace(/\*/g, '♣♦n42♠');
			replaceNoted();
			// console.log(data[i]);
		}
	}
	data = data.toString();
	data = data.replace(/\,/g, '');
	data = data.replace(/\/\*/g, '\ \/\*');
	data = data.replace(/\*\//g, '\*\/\ ');
	// data = data.replace(/\/\ \//g, '\/\/');
	// console.log(data)
	data = data.split(' ');
	for ( var i = 0; i < data.length; i++ ) {
		if ( data[i].indexOf('/*') !== -1
		&& data[i].indexOf('*/') !== -1) {
			replaceNoted();
			// console.log(data[i])
		}
	}
	
	data = data.toString();
	data = data.replace(/\,/g, '');
	// console.log(data);
	return data;
}
// Convert Note and  and class amp_cm
function resultNote(data) {
	// ### NOTE
	// ♣ => &    ♦ => #    ♠ => ;
	data = data.replace(/♣♦n64♠/g, '@');
	data = data.replace(/♣♦n58♠/g, ':');
	data = data.replace(/♣♦n59♠/g, ';');
	data = data.replace(/♣♦n46♠/g, '.');
	data = data.replace(/♣♦n123♠/g, '{');
	data = data.replace(/♣♦n125♠/g, '}');
	data = data.replace(/♣♦n34♠/g, '"');
	data = data.replace(/♣♦n35♠/g, '#');
	data = data.replace(/♣♦n36♠/g, '$');
	data = data.replace(/♣♦n39♠/g, '\'');
	data = data.replace(/♣♦n40♠/g, '(');
	data = data.replace(/♣♦n41♠/g, ')');
	data = data.replace(/♣♦n42♠/g, '\*');
	data = data.replace(/♣♦n61♠/g, '=');

	data = data.replace(/s♠note/gi, '<span class="amp_cm">')
	data = data.replace(/e♠note/gi, '</span>')
	return data;
}

// Define ' " includes SASS and Javascript
function findQuote(data) { // , ' "
	// &#123; => ♣♦123♠ => {
	data = data.replace(/\#\{/gi, '♣♦123♠') // All #{
	// &#125; => ♣♦125♠ => }

	// ', " fix error giữ lại space default '    :
	data = data.replace(/\'/g, '\ \'');	
	data = data.replace(/\"/g, '\ \"');
	data = data.split(' ');	
	for ( var i = 0; i < data.length; i++) {
		// console.log(data[i]);
		if ( data[i].slice(0,1) === '\''
		  && data[i].indexOf('♥:') !== -1
		  || data[i].slice(0,1) === '\"'
		  && data[i].indexOf('♥:') !== -1 ) {
		  	data[i] = data[i].replace('\"', '♣♦34♠e')
		  	data[i] = data[i].replace('\'', '♣♦39♠e')
			// console.log(data[i]);
		}
		// data[i] = data[i].replace('\'', '♣♦39♠e')
	}
	data = data.toString();
	data = data.replace(/\,/g, '');

	// data = data.replace(/\)\♥\{/g, '\)\{');
	// &#39; => &#39♠ => '
	// &#34; => &#34♠ => "
	// ') ']  '{  ':  ';  ', 
	data = data.replace(/\'\)/gi, '♣♦39♠e\)')
	data = data.replace(/\'\]/gi, '♣♦39♠e\]')
	data = data.replace(/\'\{/gi, '♣♦39♠e\{')
	data = data.replace(/\'\♥\{/gi, '♣♦39♠e\♥\{')

	data = data.replace(/\'\♥\+/gi, '♣♦39♠e\♥\+')
	data = data.replace(/\'\+/gi, '♣♦39♠e\+')

	data = data.replace(/\'\;/gi, '♣♦39♠e\;')
	data = data.replace(/\'\:/gi, '♣♦39♠e\:')
	data = data.replace(/\'♣♦44♠/gi, '♣♦39♠e♣♦44♠') // ',
	data = data.replace(/\'/gi, '♣♦39♠')


	data = data.replace(/\"\)/gi, '♣♦34♠e\)')
	data = data.replace(/\"\]/gi, '♣♦34♠e\]')
	data = data.replace(/\"\{/gi, '♣♦34♠e\{')
	data = data.replace(/\"\♥\{/gi, '♣♦34♠e\♥\{')

	data = data.replace(/\"\♥\+/gi, '♣♦34♠e\♥\+')
	data = data.replace(/\"\+/gi, '♣♦34♠e\+')

	data = data.replace(/\"\;/gi, '♣♦34♠e\;')
	data = data.replace(/\"\:/gi, '♣♦34♠e\:')
	data = data.replace(/\"\♣♦44♠/gi, '♣♦34♠e\♣♦44♠') // ",
	data = data.replace(/\"/gi, '♣♦34♠')
	// ♦ ♠ ♣ ♥
	// e♠var

	// console.log(data);
	
	// Special into ' ' and " "
	data = data.replace(/♣♦39♠/g, '\ ♣♦39♠');
	data = data.replace(/\ ♣♦39♠e/g, '♣♦39♠e\ ');
	data = data.replace(/♣♦34♠/g, '\ ♣♦34♠');
	data = data.replace(/\ ♣♦34♠e/g, '♣♦34♠e\ ');
	data = data.split(' ');
	var newData = [];
	data.forEach( function(e) {
		if ( e.indexOf('♣♦39♠') !== -1 || e.indexOf('♣♦34♠') !== -1 ) {
			e = e.replace(/\//g, '♣♦47♠'); // ♣♦47♠ => /
			e = e.replace(/\@/g, '♣♦n64♠'); // ♣♦n64♠ => @
			// console.log(e);
		}
		if ( e.indexOf('♣♦39♠') !== -1 && e.indexOf('\n') !== -1 ) {
			e = e.replace('♣♦39♠', '♣♦39♠e'); // fix quote file.json
			// console.log(e);
		}
		if ( e.indexOf('♣♦34♠') !== -1 && e.indexOf('\n') !== -1 ) {
			e = e.replace('♣♦34♠', '♣♦34♠e'); // fix quote file.json
			// console.log(e);
		}
		// console.log(e);
		newData.push(e);
	});
	data = newData;
	data = data.toString();
	data = data.replace(/\,/g, '');
	// console.log(data);
	return data;
}

// Define Method includes Javascript
function findMethod(data) {
	var maxSpace = fn_findSpace(data);
	data = data.replace(/\ /g, '\♥');
	data = data.replace(/\./g, '\.\ ');
	data = data.replace(/\(/g, '\(\ ');
	data = data.replace(/\{/g, '\{\ ');
	data = data.replace(/\}/g, '\}\ ');
	data = data.replace(/\;/g, '\;\ ');
	data = data.replace(/\|/g, '\|\ ');
	data = data.replace('s♠script', 's♠script\ ');
	// console.log(data);
	// data = data.replace(/function/g, '\ function');
	// console.log(data);
	var jsData = ['for', 'do', 'while', 'break', 'if', 'else', 'return', 'switch']; // amp_attr with (  || amp_vl with {
	var jsDataAfter = ['do', 'else', 'typeof', 'return', 'break']; // amp_vl

	jsData.forEach( function(e) {
		if ( data.indexOf(e) !== -1 ) {
			data = data.replace(new RegExp(e, "g"), ' ' + e);
			// console.log(e);
		}
	});
	// console.log(data);

	// console.log(data);

	data = data.split(' ');
	var newData = [];
	data.forEach( function(e) {
		if ( e.indexOf('(') !== -1 ) {
			// RESET SPANCE HTML
			function resetFormat(data) {
				var i = 0;
				while (i <= maxSpace) {
					data = data.replace(/\♥\(/g, '\_\(');
					data = data.replace(/\♥\_/g, '\_\_');
					i++;
				}
				return data;
			}
			e = resetFormat(e);
			// console.log(e);
			jsData.forEach( function(el){ // Data method() default javascript
				if ( e.indexOf(el+'_') !== -1
				|| e.indexOf(el+'(') !== -1 ) {
					e = e.replace(/\_/g, '♥');
					e = e.replace('(', '</span>(')
					e = '<span♥class="amp_vl">' + e;
					// console.log(e);
				}
			});
			if ( e.indexOf('amp_vl') === -1
			  && e.indexOf('♥') !== -1
			  && e.indexOf('$') === -1 ) {
				var last = e.lastIndexOf('♥');
				e = e.replace(/\_/g, '♥');
				e = e.replace('(', '</span>(')
				e = e.slice(0, last) + '<span♥class="amp_attr">' + e.slice(last+1, e.length);
				// console.log(e);
			}
			// Inlcude alphabate
			if ( e.indexOf('amp_vl') === -1 && alphabet.indexOf(e.slice(0,1)) !== -1 ) {
				e = e.replace('(', '</span>(')
				e = '<span♥class="amp_attr">' + e;
				// console.log(e);
			}
			// RESET SPANCE HTML
			if ( e.indexOf('amp_vl') === -1
				&& e.indexOf('amp_attr') === -1 
				&& e.indexOf('_(') !== -1 ) {
				e = e.replace(/\_/g, '♥');
				// console.log(e);
			}
			// console.log(e);
		}
		if ( e.indexOf('{') !== -1 ) {
			jsDataAfter.forEach( function(el) { // Data default javascript width {
				if ( e.indexOf(el) !== -1 ) {
					e = e.replace('{', '</span>{')
					e = '<span♥class="amp_vl">' + e;
					// console.log(e);
				}
				// console.log(e);
			});
		}
		if ( e.indexOf('amp_vl') === -1
		  && e.indexOf('.') === -1 ) {
			jsDataAfter.forEach( function(el) { // Data default javascript width space
				if ( e.indexOf(el) !== -1 ) {
					if (e.indexOf('♥') !== -1 ) {
						var last = e.lastIndexOf('♥');
						e = e.slice(0, last) + '</span>♥' + e.slice(last+1, e.length);
						e = '<span♥class="amp_vl">' + e;
						// console.log(e);
					}
					if (e.indexOf(';') !== -1 ) {
						e = e.replace(';', '</span>;');
						e = '<span♥class="amp_vl">' + e;
						// console.log(e);
					}
					// console.log(e);
				}
			})
			// console.log(e);
		}
		newData.push(e);
	});
	data = newData;
	// console.log(data);
	data = data.toString();
	// data = data.replace(/\,/g, '');
	// console.log(data);
	return data;
}

// Define $ $var includes SASS and Javascript
function findVar(data) {
	data = addSpace(data);
	// console.log(data);
	data = data.replace(/\$/gi, '\ \$')
	data = data.replace(/\ \}/gi, '\}') // => ♣♦125♠ }
	// console.log(data);
	data = data.split(' ');
	// e♠var => </span>
	for ( var i = 0; i < data.length; i++ ) {
		// $♥
		// console.log(data[i])
		if ( data[i].indexOf('$') !== -1 ) {
			data[i] = data[i].replace('\$', 's♠var');
			if ( data[i].indexOf('\}') !== -1 ) {
				data[i] = data[i].replace('\}', 'e♠var\♣♦125♠'); // \}
				// console.log(data[i])
			}
			if ( data[i].indexOf('\♥') !== -1
			  && data[i].indexOf('e♠var') === -1 ) {
				data[i] = data[i].replace('\♥', 'e♠var\♥'); // \♥
				// console.log(data[i])
			}
			if ( data[i].indexOf('e♠var') === -1 ) {
				data[i] = data[i] + 'e♠var';
				// console.log(data[i])
			}
			// console.log(data[i])
		}
	}
	// convert to string
	data = data.toString();
	data = data.replace(/\,/gi, '')
	// console.log(data)
	return data;
}

// Add space for special tag
function addSpace(data) {
	data = data.replace(/\[/gi, '\ \[\ ')
	data = data.replace(/\(/gi, '\ \(\ ')
	data = data.replace(/\{/gi, '\ \{\ ')
	data = data.replace(/\#/gi, '\ \#\ ')
	data = data.replace(/\./gi, '\ \.\ ')
	data = data.replace(/\@/gi, '\ \@\ ')
	data = data.replace(/\]/gi, '\ \]\ ')
	data = data.replace(/\)/gi, '\ \)\ ')
	data = data.replace(/\}/gi, '\ \}\ ')
	data = data.replace(/\:/gi, '\ \:\ ')
	data = data.replace(/\;/gi, '\ \;\ ')
	data = data.replace(/\>/gi, '\ \>\ ')
	data = data.replace(/\</gi, '\ \<\ ')
	data = data.replace(/\+/gi, '\ \+\ ')
	data = data.replace(/\*/gi, '\ \*\ ')
	data = data.replace(/\=/gi, '\ \=\ ')
	data = data.replace(/\~/gi, '\ \~\ ')
	data = data.replace(/♣♦123♠/gi, '\ ♣♦123♠\ ')
	data = data.replace(/♣♦44♠/gi, '\ ♣♦44♠\ ')
	return data;
}

// Convert special text
function resultSassCss(data) {
	data = data.replace(/\♥/gi, ' ');
	// &sbquo; => ♣♦44♠ => ,
	// data = data.replace(/\,/gi, '♣♦44♠') // All ,
	// data = data.replace(/♣♦44♠/gi, '<span class="amp_df">\,</span>')

	// ### Quote
	// &#34; => ♣♦34♠e => "
	// &#39; => ♣♦39♠e => '
	data = data.replace(/♣♦34♠e/gi, '\"</span>')
	data = data.replace(/♣♦34♠/gi, '<span class="amp_q">\"')
	data = data.replace(/♣♦39♠e/gi, '\'</span>')
	data = data.replace(/♣♦39♠/gi, '<span class="amp_q">\'')
	data = data.replace(/♣♦47♠/gi, '\/');

	// ### VAR #{$var}
	// &♠123♠ => #{
	// &♠125♠ => }
	data = data.replace(/♣♦123♠/gi, '<span class="amp_df">\#\{')
	data = data.replace(/♣♦125♠/gi, '\}\</span>')
	data = data.replace(/s♠var/gi, '<span class="amp_var">\$')
	data = data.replace(/e♠var/gi, '</span>')

	// TAG
	data = data.replace(/s♠tag/gi, '<span class="amp_tag">')
	data = data.replace(/e♠tag/gi, '</span>')
	// @MEDIA ..
	data = data.replace(/\@\@/gi, '\@♣♦n64♠')
	data = data.replace(/@/gi, '<span class="amp_fn">\@')
	data = data.replace(/e♠fn/gi, '</span>')

	// VALUE :____;
	data = data.replace(/s♠vl/gi, '<span class="amp_vl">')
	data = data.replace(/e♠vl/gi, '</span>')

	// // ###
	// // SELECTOR :hover
	data = data.replace(/s♠sl/gi, '<span class="amp_sl">')
	data = data.replace(/e♠sl/gi, '</span>')

	// // SELECTOR CLASS & ID .class #id
	data = data.replace(/s♠c♠id/g, '<span class="amp_attr">')
	data = data.replace(/e♠c♠id/g, '</span>')

	// []
	data = data.replace(/\[/gi, '<span class="amp_df">\[')
	data = data.replace(/\]/gi, '\]\</span>')
	// ()
	data = data.replace(/\(/gi, '\(<span class="amp_df">')
	data = data.replace(/\)/gi, '</span>\)')

	// // PROPERTY
	data = data.replace(/s♠pr/gi, '<span class="amp_pr">')
	data = data.replace(/e♠pr/gi, '</span>')
	return data;
}

// Define css include style tag
function findStyleCss($language, $label, $colors, $attr, data) {
	data = data.replace(/\,/gi, '♣♦44♠') // All ,
	data = data.replace(/\ /g, '\♥');
	data = data.replace(/\<style/g, '\ \<style');
	data = data.replace(/\<\/style/g, '\ \<\/style');
	data = data.split(' ');
	for (var i = 0; i < data.length; i++) {
		if ( data[i].indexOf('<style') !== -1 ) {
			data[i] = data[i].replace('>', '\>s♠style');
		}
	}
	data = data.toString();
	data = data.replace(/\,/g, '');
	data = data.replace(/>s♠style/g, '\>\ s♠style');
	data = data.replace(/\<\/style/g, '\ \<\/style');
	data = data.split(' ');
	for (var i = 0; i < data.length; i++) {
		if ( data[i].indexOf('s♠style') !== -1 ) {
			// data[i] = data[i].replace('s♠style', '');
			data[i] = convertCSS($language, $label, $colors, $attr, data[i]);
			data[i] = data[i].replace(/s♠style/g, '');
			data[i] = 's♠style' + data[i];
			// console.log(data[i]);
		}
	}
	data = data.toString();
	// console.log(data)
	data = data.replace(/\,/g, '');
	// console.log(data);
	return data;
}

// Define script includes html
function findScript(data) {
	// console.log(data);
	data = data.replace(/\,/gi, '♣♦44♠') // All ,
	data = data.replace(/\ /g, '\♥');
	// RESET SPANCE HTML
	var maxSpace = fn_findSpace(data);
	function resetFormat(data) {
		var i = 0;
		while (i <= maxSpace) {
			data = data.replace(/\♥\♥\+/g, '\♥\+');
			data = data.replace(/\+\♥\♥/g, '\+\♥');
			// data = data.replace(/\♥\♥\(/g, '\♥\(');
			i++;
		}
		// console.log(data);
		return data;
	}
	data = resetFormat(data);
	// console.log(data);
	
	data = data.replace(/\<script/g, '\ \<script');
	data = data.replace(/\<\/script/g, '\ \<\/script');

	data = data.split(' ');
	for (var i = 0; i < data.length; i++) {
		if ( data[i].indexOf('<script') !== -1 ) {
			data[i] = data[i].replace('>', '\>s♠script');
		}
	}
	data = data.toString();
	data = data.replace(/\,/g, '');
	data = data.replace(/>s♠script/g, '\>\ s♠script');
	data = data.replace(/\<\/script/g, '\ \<\/script');

	data = data.split(' ');
	for (var i = 0; i < data.length; i++) {
		if ( data[i].indexOf('s♠script') !== -1 ) {
			data[i] = data[i].replace(/\"\>/gi, '♣♦34♠e\>')
			data[i] = data[i].replace(/\'\>/gi, '♣♦39♠e\>')
			data[i] = data[i].replace(/\>/g, '&gt;');
			data[i] = data[i].replace(/\</g, '&lt;');
			data[i] = data[i].replace(/\=/g, '&#61;');

			// console.log(data[i]);

			data[i] = findQuote(data[i]);
			data[i] = findNoted(data[i]);
			// console.log(data[i]);

			data[i] = findVar(data[i]);

			data[i] = resultSassCss(data[i]);
			data[i] = resultNote(data[i]);

			// console.log(data[i]);

			data[i] = findMethod(data[i]);
			data[i] = data[i].replace(/function/g, '<span class="amp_attr">function</span>');
			data[i] = data[i].replace(/var♥/g, '<span class="amp_attr">var</span>♥');
		}
	}
	data = data.toString();
	// console.log(data)
	data = data.replace(/\,/g, '');
	// data = data.replace(/\=/g, '\=\ ');
	// console.log(data);
	return data;
}
// END GLOBAL FUNCTION


// ############################## //
// FUNCTION convertSCSS
function convertSCSS($language, $label, $colors, $attr, $data) {
	var dataScss = getText.value;
	var special = ['\'', '\"', ',', '{', '}', '[', ']', '(', ')']
	if ( $data != undefined ) {
		dataScss = $data;
	}
	var maxSpace = fn_findSpace(dataScss);
	// console.log(maxSpace);
	// Clear space for special
	function resetFormat(data) {
		var i = 0;
		data = data.replace(/\♥/g, ' ');
		while (i <= maxSpace) {
			data = data.replace(/\'\ \ /g, '\'\ ');
			data = data.replace(/\"\ \ /g, '\"\ ');

			data = data.replace(/\(\ /g, '\(');
			data = data.replace(/\ \)/g, '\)');

			data = data.replace(/\[\ /g, '\[');
			data = data.replace(/\ \]/g, '\]');
			data = data.replace(/\ \,/g, '\,');
			data = data.replace(/\ \;/g, '\;');

			data = data.replace(/\@\ /g, '\@');
			i++;
		}
		data = data.replace(/\ /g, '\♥');
		return data;
	}
	dataScss = resetFormat(dataScss);
	// console.log(data);

	// FUNCTION findNoted();
	dataScss = findNoted(dataScss);
	// console.log(dataScss);

	// Quote console.log(data); // Global
	dataScss = findQuote(dataScss);

	// console.log(data); // Global
	dataScss = findVar(dataScss);

	function findTag(data) {
		data = data.replace(/\{/gi, '\{\ ');
		data = data.replace(/\}/gi, '\}\ ');
		data = data.replace(/\;/gi, '\;\ ');
		data = data.replace(/\@/gi, '\ \@');
		data = data.replace(/e♠note/gi, 'e♠note\ ');
		// console.log(data);
		data = data.split(' ');
		for ( var i = 0; i < data.length; i++ ) {
			// s♠tag tag e♠var{}
			if ( data[i].indexOf('{') !== -1
			  && data[i].indexOf('@') === -1 ) {
				data[i] = 's♠tag' + data[i] + 'e♠tag';
				data[i] = data[i].replace(/\{e♠tag/gi, 'e♠tag\{');
				// console.log(data[i]);
			}

			// ##### Define value css and value into var sass
			// Value  :---;   none ();
			if ( data[i].indexOf(':') !== -1 && data[i].indexOf(';') === -1 ) {
				// no select $var:(); || $var: ();
				if ( data[i].match(/:/g).length == 1 ) {
					data[i] = data[i].replace(/\:/g, 'e♠pr\:s♠vl');
					data[i] = data[i].replace('\;', 'e♠vl\;');
					// console.log(data[i]);
				}
				// console.log(data[i]);

				// option $var:( property:value, property:value );
				// create space ♠v♠  $var:(♠v♠ property:value, property:value ♠v♠);
				if ( data[i].indexOf('e♠pr') === -1
				  && data[i].match(/:/g).length > 1 ) {
					data[i] = data[i].replace(/♣♦44♠/g, 'e♠vl♣♦44♠♠v♠'); // after ,
					data[i] = data[i].replace('(', '\(♠v♠');                 // first (
					data[i] = data[i].replace(/\)\;/g, 'e♠vl♠v♠\)\;');       // after last );
					// console.log(data[i]);
				}
			}
			// #####

			// @media, @for, @if ... funtion amp_fn
			if ( data[i].indexOf('@') !== -1 ) {
				data[i] = data[i].replace('♥', 'e♠fn\♥');
				// console.log(data[i]);
			}
			// @media + (property: value);
			if ( data[i].indexOf(':') !== -1
			  && data[i].indexOf('@') !== -1 ) {
			  	data[i] = data[i].replace('(', '\(s♠pr');
			  	data[i] = data[i].replace(')', 'e♠vl\)');
			  	data[i] = data[i].replace(':', 'e♠pr\:s♠vl');
				// console.log(data[i]);
			}

			// Selector
			// :♥   :(   :{   || no :&
			if ( data[i].indexOf('{') !== -1
			  && data[i].indexOf(':') !== -1
			  && data[i].indexOf('@') === -1 ) {
			  	// Còn lại 2 trường hợp :hover tag | :hover > tag     :hover{
				// s♠sl
				// console.log(data[i]);
			  	data[i] = data[i].replace(/\:/gi, 's♠sl\:');                // add all s♠sl:seletor 
			  	data[i] = data[i].replace(/\:s♠sl\:/gi, '\:\:');            // fix ::after ::before ...
			  	data[i] = data[i].replace(/\(/gi, 'e♠sl\(');               // add s♠sl:seletor with (  :not()
			  	// data[i] = data[i].replace(/\♣♦44♠/gi, 'e♠sl\♣♦44♠');   // add s♠sl:seletor, with (  :focus,
			  	// data[i] = data[i].replace(/\{/gi, 'e♠sl\{');
				// console.log(data[i]);
			}
		}
		// convert to string
		data = data.toString();
		// console.log(data)
		data = data.replace(/\,/gi, '')
		return data;
	}
	dataScss = findTag(dataScss);


	// console.log(data); // FindTag
	function PropertyVSSelector(data) {

		data = data.replace(/\{/gi, '\{\ ');
		data = data.replace(/\}/gi, '\}\ ');

		data = data.replace(/♠v♠/gi, '\ ');

		data = data.replace(/\;/gi, '\;\ ');
		data = data.replace(/\n/gi, '\n\ ');
		data = data.replace(/\./gi, '\ \.');
		data = data.replace(/\#/gi, '\ \#');
		data = data.replace(/\#\ \#/gi, '\#\#');
		data = data.replace(/s♠sl/gi, '\ \s♠sl');
		data = data.replace(/s♠vl/gi, '\ \s♠vl');
		// console.log(data)

		data = data.split(' ');
		// console.log(data)
		// property:
		for ( var i = 0; i < data.length; i++ ) {
			// option $var:(property:value, property:value);
			if ( data[i].indexOf(':') !== -1   
			  && data[i].indexOf('e♠vl') !== -1 ) {
			   	data[i] = 's♠pr' + data[i];
			  	data[i] = data[i].replace(/\:/gi, 'e♠pr:s♠vl');
				// console.log(data[i]);
			}
			if ( data[i].indexOf(':') !== -1
			&& data[i].indexOf('s♠sl') === -1
			&& data[i].indexOf('e♠pr') === -1
			&& data[i].slice(data[i].length - 1, data[i].length) != '(' ) {
			   	data[i] = 's♠pr' + data[i];
			  	data[i] = data[i].replace(/\:/gi, 'e♠pr:s♠vl');
				// console.log(data[i]);
			}
			if ( data[i].indexOf('e♠pr') !== -1
			  && data[i].indexOf('s♠pr') === -1 ) {
			  	data[i] = 's♠pr' + data[i];
				// console.log(data[i]);
			}
			// console.log(data[i]);
		}
		// :selector
		for ( var i = 0; i < data.length; i++ ) {
			if ( data[i].indexOf('s♠sl') !== -1       // no :selector đã khai báo ở trên
			  && data[i].indexOf('e♠sl') === -1 ) { // no selector with a:#{$var}
			  	data[i] = data[i].replace('\♥', 'e♠sl\♥');
			  	// console.log(data[i]);
			  	if ( data[i].indexOf('e♠sl') === -1 ) {
			  		data[i] = data[i].replace('\{', 'e♠sl\{'); // selector :hover {
			  		data[i] = data[i].replace(/\♣♦44♠/gi, 'e♠sl\♣♦44♠');   // add s♠sl:seletor, with (  :focus,
			  		// console.log(data[i]);
			  	}
			}
		}

		// .class #id
		// s♠sl   e♠var   &♠   class va id the same selector
		for ( var i = 0; i < data.length; i++ ) {
			if ( checkNum.indexOf(data[i].slice(1,2)) === -1  // no class or id number .15
			  	&& data[i].indexOf('e♠vl') === -1             // no class or id into value
			  	&& data[i].indexOf('♣♦39♠e') === -1           // no class or id into quote
			  	&& data[i].indexOf(';') === -1 ) {
			  	if ( data[i].slice(0,1) === '.' || data[i].slice(0,1) === '#' ) {
			  		data[i] = data[i].replace('#', 's♠c♠id\#');
			  		data[i] = data[i].replace('.', 's♠c♠id\.');
				  	data[i] = data[i].replace('♥', 'e♠c♠id\♥');             // after \♥
			  		// console.log(data[i]);
			  		if ( data[i].indexOf('e♠c♠id') === -1 ) { // no space with more options
			  			if ( data[i].indexOf('e♠tag') !== -1 ) {
				  			data[i] = data[i].replace('e♠tag', 'e♠c♠id\e♠tag');   // before {
						}
						if ( data[i].indexOf('♣♦44♠') !== -1 ) {
				  			data[i] = data[i].replace('♣♦44♠', 'e♠c♠id\♣♦44♠'); // before ,
						}
			  		}
			  		if ( data[i].indexOf('e♠c♠id') === -1 ) {
			  			data[i] = data[i] + 'e♠c♠id'; // after no tag is space
			  			// console.log(data[i]);
			  		}
			  	}
			}
			if ( data[i].indexOf('.') !== -1 || data[i].indexOf('#') !== -1 ) {
				// console.log(data[i]);
			}
		}
		data = data.toString();
		// console.log(data);
		data = data.replace(/\,/gi, '')
		return data;
	}
	dataScss = PropertyVSSelector(dataScss);

	// ### RESULT SPECIAL TEXT
	dataScss = resultSassCss(dataScss)

	// ### RESULT NOTE FUNCTION
	dataScss = resultNote(dataScss);

	// console.log('############################');
	function finalScss(data) {
		// Select label
		if ( $label === 'ON') {
			outputText.value = '<div class="'+ $colors +'"><abbr title="'+ $attr +'" class="amp_tag_abbr">'+$language+'</abbr><pre class="amp_pre_wrap">'+data+'</pre></div>';
			showText.innerHTML = '<div class="'+$colors+'"><abbr title="'+ $attr +'" class="amp_tag_abbr">'+$language+'</abbr><pre class="amp_pre_wrap">'+data+'</pre></div>';
		}
		if ( $label === 'OFF') {
			outputText.value = '<div class="'+ $colors +'"><pre class="amp_pre_wrap">'+data+'</pre></div>';
			showText.innerHTML = '<div class="'+$colors+'"><pre class="amp_pre_wrap">'+data+'</pre></div>';
		}
	}
	
	if ( $data != undefined ) {
		return dataScss;
	} else {
		dataScss = dataScss.replace(/♣♦44♠/gi, '<span class="amp_df">\,</span>')
		finalScss(dataScss);
	}
}

// FUNCTION convertCSS
function convertCSS($language, $label, $colors, $attr, $data) {
	var dataCss = getText.value;

	if ( $data != undefined ) {
		dataCss = $data;
	}
	// console.log(dataCss);
	var maxSpace = fn_findSpace(dataCss);

	// ### resetFormat ###
	function resetFormat(data) {
		var i = 0;
		data = data.replace(/\,/gi, '♣♦44♠') // All ,
		data = data.replace(/\♥/g, ' ');
		while (i <= maxSpace) {
			data = data.replace(/\'\ /g, '\'');
			data = data.replace(/\ \ \'/g, '\ \'');
			data = data.replace(/\"\ /g, '\"');
			data = data.replace(/\ \ \"/g, '\ \"');
			// css + js
			data = data.replace(/\(\ /g, '\(');
			data = data.replace(/\ \)/g, '\)');
			data = data.replace(/\[\ /g, '\[');
			data = data.replace(/\ \]/g, '\]');
			data = data.replace(/\ \,/g, '\,');
			data = data.replace(/\ \;/g, '\;');
			data = data.replace(/\@\ /g, '\@');
			i++;
		}
		data = data.replace(/\:\"/g, '\:\ \"');
		data = data.replace(/\:\'/g, '\:\ \'');
		// data = data.replace(/\ /g, '\♥');
	 	// console.log(data);
	 	return data;
	}
	dataCss = resetFormat(dataCss);


	// ### FUNCTION findNoted(); ###
	dataCss = findNoted(dataCss);

	// FINDQUOTE
	function findQuote(data) {
		// ♣ => &   ♦ => #    ♠ => ;
		// &#34; => ♣♦34♠ => "
		// &#39; => ♣♦39♠ => '
		// ')  ']   ';
		data = data.replace(/\'\)/gi, '♣♦39♠e\)')
		data = data.replace(/\'\]/gi, '♣♦39♠e\]')
		data = data.replace(/\'\;/gi, '♣♦39♠e\;')
		data = data.replace(/\'\♣♦44♠/gi, '♣♦39♠e\♣♦44♠') // ',
		data = data.replace(/\'/gi, '♣♦39♠')

		data = data.replace(/\"\)/gi, '♣♦34♠e\)')
		data = data.replace(/\"\]/gi, '♣♦34♠e\]')
		data = data.replace(/\"\;/gi, '♣♦34♠e\;')
		data = data.replace(/\"\♣♦44♠/gi, '♣♦34♠e\♣♦44♠') // ',
		data = data.replace(/\"/gi, '♣♦34♠')
		return data;
		// console.log(dataCss);
	}
	dataCss = findQuote(dataCss);

	// ### findTag ###
	function findTag(data) {
		data = data.replace(/\ /g, '\♥');
		data = data.replace(/\{/g, '\{\ ');
		data = data.replace(/\}/g, '\}\ ');
		data = data.replace(/\;/g, '\;\ ');
		data = data.replace(/\@/g, '\ \@');
		data = data.split(' ');
		for ( var i = 0; i< data.length; i++ ) {
			if ( data[i].indexOf('{') !== -1
			&& data[i].indexOf('@') === -1 ) {
				data[i] = 's♠tag' + data[i];
				data[i] = data[i].replace(/\{/gi, 'e♠tag\{');
				// console.log(data[i]);
			}
			// Find Fn
			if ( data[i].indexOf('@') !== -1 ) {
				data[i] = data[i].replace('♥', 'e♠fn♥');
				data[i] = data[i].replace(':', 'e♠pr:s♠vl');
				data[i] = data[i].replace(')', 'e♠vl\)');
				data[i] = data[i].replace('(', '\(s♠pr');
				data[i] = data[i].replace('♣♦', 'e♠fn♣♦');
				// console.log(data[i]);
			}
			// Selector :value;
			if ( data[i].indexOf(':') !== -1
			&& data[i].indexOf(';') !== -1 ) {
				data[i] = data[i].replace(';', 'e♠vl\;');
				data[i] = data[i].replace(':', 'e♠pr:s♠vl');
				// console.log(data[i]);
			}
			// Selector :hover
			if ( data[i].indexOf(':') !== -1
			&& data[i].indexOf('e♠pr') === -1 ) {
				data[i] = data[i].replace(/\(/g, 'e♠sl(');          // (
				data[i] = data[i].replace(/♣♦44♠/g, 'e♠sl♣♦44♠');   // ,
				data[i] = data[i].replace(/\:/g, 's♠sl:');          // :
				data[i] = data[i].replace(/:s♠sl:/g, '::');         // ::
				// console.log(data[i]);
			}
		}
		data = data.toString();
		data = data.replace(/\,/g, '');
		// console.log(data);
		return data;
	}
	dataCss = findTag(dataCss);
	// console.log(dataCss);

	function findProperty(data) {
		data = data.replace(/\{/g, '\{\ ');
		data = data.replace(/\;/g, '\;\ ');
		data = data.replace(/\./g, '\ \.');
		data = data.replace(/\#/g, '\ \#');
		data = data.replace(/e♠pr:/g, 'e♠pr:\ ');
		data = data.replace(/s♠sl:/g, '\ s♠sl:');
		data = data.split(' ');
		for ( var i = 0; i < data.length; i++ ) {
			// Property
			if ( data[i].indexOf('e♠pr') !== -1 && data[i].indexOf('s♠pr') === -1 ) {
				if ( data[i].indexOf('♥') !== -1 ) {
					data[i] = data[i].replace(/\♥/g, '\♥\ ');
			    	data[i] = data[i].replace(/ \♥/g, '\♥');
					data[i] = data[i].replace('\♥\ ', 's♠pr\♥');
					// console.log(data[i]);
				} else {
					data[i] = 's♠pr' + data[i];
				}
				// console.log(data[i]);
			}
			// Selector :hover
			if ( data[i].indexOf('s♠sl') !== -1 ) {
				if ( data[i].indexOf('♥') !== -1 ) {
					data[i] = data[i].replace('♥', 'e♠sl♥');
					// console.log(data[i]);
				}
				if ( data[i].indexOf('e♠sl') === -1 ) {
					data[i] = data[i].replace('e♠', 'e♠sle♠');
					// console.log(data[i]);
				}
			}
			// Class and ID
			if ( data[i].indexOf(';') === -1 && checkNum.indexOf(data[i].slice(1,2)) === -1 ) {
				if ( data[i].slice(0,1) === '.' || data[i].slice(0,1) === '#' ) {
					if ( data[i].indexOf('{') !== -1 && data[i].indexOf('♥') !== -1 ) {
				  		data[i] = 's♠c♠id' + data[i];
				  		data[i] = data[i].replace('♥', 'e♠c♠id\♥');
				  		// console.log(data[i]);
				  	}
				  	if ( data[i].indexOf('e♠c♠id') === -1 && data[i].indexOf('{') !== -1 ) {
				  		data[i] = 's♠c♠id' + data[i];
				  		data[i] = data[i].replace('\{', 'e♠c♠id\{');
				  		// console.log(data[i]);
				  	}
				  	if ( data[i].indexOf('e♠c♠id') === -1 && data[i].indexOf('♥') === -1 ) {
				  		data[i] = 's♠c♠id' + data[i] + 'e♠c♠id';
				  		// console.log(data[i]);
				  	}
				  	if ( data[i].indexOf('e♠c♠id') === -1) {
				  		data[i] = 's♠c♠id' + data[i];
				  		data[i] = data[i].replace('♥', 'e♠c♠id\♥');
				  		// console.log(data[i]);
				  	}
					// console.log(data[i]);
				}
				// console.log(data[i]);
			}
		}
		data = data.toString();
		data = data.replace(/\,/g, '');
		return data;
	}
	dataCss = findProperty(dataCss);

	// ### RESULT SPECIAL TEXT
	dataCss = resultSassCss(dataCss)
	// ### RESULT NOTE FUNCTION
	dataCss = resultNote(dataCss);


	function finalCss(data) {
		// Select label
		if ( $label === 'ON') {
		outputText.value = '<div class="'+ $colors +'"><abbr title="'+ $attr +'" class="amp_tag_abbr">'+$language+'</abbr><pre class="amp_pre_wrap">'+data+'</pre></div>';
		showText.innerHTML = '<div class="'+$colors+'"><abbr title="'+ $attr +'" class="amp_tag_abbr">'+$language+'</abbr><pre class="amp_pre_wrap">'+data+'</pre></div>';
		}
		if ( $label === 'OFF') {
			outputText.value = '<div class="'+ $colors +'"><pre class="amp_pre_wrap">'+data+'</pre></div>';
			showText.innerHTML = '<div class="'+$colors+'"><pre class="amp_pre_wrap">'+data+'</pre></div>';
		}
	}
	// finalCss(dataCss);

	if ( $data != undefined ) {
		return dataCss;
	} else {
		dataCss = dataCss.replace(/♣♦44♠/gi, '<span class="amp_df">\,</span>')
		finalCss(dataCss);
	}
}

// FUNCTION convertHTML
function convertHTML($language, $label, $colors, $attr) {
	var dataHtml = getText.value;

	var maxSpace = fn_findSpace(dataHtml);
	// console.log(maxSpace);

	// DEFINE CSS STYLE
	dataHtml = findStyleCss($language, $label, $colors, $attr, dataHtml);

	// DEFINE SCRIPT
	dataHtml = findScript(dataHtml);

	// console.log(dataHtml);

	function resetFormat(data) {
		data = data.replace(/\,/gi, '♣♦44♠') // All ,
		var i = 0;
		data = data.replace(/\♥/g, ' ');
		while (i <= maxSpace) {
			data = data.replace(/\=\ /g, '\=');
			data = data.replace(/\<\ /g, '\<');
			data = data.replace(/\<\/\ /g, '\<\/');
			i++;
		}
		// console.log(data);
		// data = data.replace(/\ /g, '\♥');
		return data;
	}
	dataHtml = resetFormat(dataHtml);
	// console.log(dataHtml);

	// FIND TAG HTML
	function findTag(data) {
		data = data.replace(/\</g, '\ \<');
		data = data.split(' ');
		var newData = [];
		data.forEach( function(e) {
			e = e.replace(/\<\!\-/g, 's♠cm');
			e = e.replace(/\-\-\>/g, 'e♠cm');
			e = e.replace('<', 's♠tag');
			e = e.replace('>', 'e♠tag');
			newData.push(e);
		});
		data = newData;
		data = data.toString();
		data = data.replace(/\,/g, '');
		return data
	}

	// FIND ATTRIBUTE & VALUE
	function findAttr(data) {
		data = data.replace(/\=\"/g, '\=\"\ ');
		data = data.replace(/\=\'/g, '\=\'\ ');
		// data = data.replace(/\=/g, '\=\ ');
		data = data.split(' ');
		// console.log(data);
		var newData = [];
		data.forEach( function(e) {
			// lastIndexOf find number last charset
			if ( e.indexOf('=') !== -1 ) {
				var last = e.lastIndexOf('♥');
				e = e.slice(0, last) + '♥s♠attr' + e.slice(last+1, e.length);
				e = e.replace('\=\'', '=e♠attrs\♠q39');
				e = e.replace('\=\"', '=e♠attrs\♠q34');
				// console.log(e);
			}
			e = e.replace(/\"/g, 'e♠q34');
			e = e.replace(/\'/g, 'e♠q39');
			newData.push(e);
		});
		data = newData;
		data = data.toString();
		data = data.replace(/\,/g, '');
		return data
	}

	function hideCssScript(data) {
		data = data.replace(/\ /g, '♥');
		data = data.replace(/s♠style/g, '\ s♠style');
		data = data.replace(/\<\/style/g, '\ \<\/style');

		data = data.replace(/>s♠script/g, '\>\ s♠script');
		data = data.replace(/\<\/script/g, '\ \<\/script');

		data = data.split(' ');
		// console.log(data);
		var newData = [];
		data.forEach( function(e) {
			if ( e.indexOf('s♠style') === -1 && e.indexOf('s♠script') === -1 ) {

				e = findTag(e);
				e = findAttr(e);

				// QUOTE VALUE
				e = e.replace(/e♠q34/g, '\"</span>');                       //e♠q34e♠ \"
				e = e.replace(/s♠q34/g, '<span class="amp_vl">\"');         // \'
				e = e.replace(/e♠q39/g, '\'</span>');                       //e♠q39e♠ \"
				e = e.replace(/s♠q39/g, '<span class="amp_vl">\'');         // \'

				// TAG
				e = e.replace(/e♠tag/g, '&gt;</span>');                  //e♠q39e♠ \"
				e = e.replace(/s♠tag/g, '<span class="amp_tag">&lt;');   // \'

				// ATTRIBUTE
				e = e.replace(/e♠attr/g, '</span>');                    //
				e = e.replace(/s♠attr/g, '<span class="amp_attr">');    //

				// COMMENTS
				e = e.replace(/e♠cm/g, '\-\-\&gt;</span>');
				e = e.replace(/s♠cm/g, '<span class="amp_cm">\&lt;\!\-');
				// console.log(e);
			}
			e = e.replace('s♠style', '');
			e = e.replace('s♠script', '');
			// console.log(e);
			newData.push(e);
		});
		data = newData;
		data = data.toString();
		data = data.replace(/\,/g, '');
		data = data.replace(/♣♦44♠/g, '<span class="amp_df">\,</span>');
		data = data.replace(/!DOCTYPE/g, '!<span class="amp_attr">DOCTYPE</span>');
		data = data.replace(/\♥/g, ' ');
		// console.log(data);
		return data;
	}
	dataHtml = hideCssScript(dataHtml);
	// console.log(dataHtml);
	
	if ( $label === 'ON') {
		outputText.value = '<div class="'+ $colors +'"><abbr title="'+ $attr +'" class="amp_tag_abbr">'+$language+'</abbr><pre class="amp_pre_wrap">'+dataHtml+'</pre></div>';
		showText.innerHTML = '<div class="'+$colors+'"><abbr title="'+ $attr +'" class="amp_tag_abbr">'+$language+'</abbr><pre class="amp_pre_wrap">'+dataHtml+'</pre></div>';
	}
	if ( $label === 'OFF') {
		outputText.value = '<div class="'+ $colors +'"><pre class="amp_pre_wrap">'+dataHtml+'</pre></div>';
		showText.innerHTML = '<div class="'+$colors+'"><pre class="amp_pre_wrap">'+dataHtml+'</pre></div>';
	}	
}
// ############################## //


// ##### FUNCTION CHOOSE OPTIONS #####
var getL = document.getElementById('language');
var getLable = document.getElementById('labels');
var getColors = document.getElementById('colors');

function actionConvert() {
	// Select Language
	var xL = getL.selectedIndex;
	var yL = getL.options[xL].value;
	// console.log(yL);

	// Select Lables
	var xLa = getLable.selectedIndex;
	var yLa = getLable.options[xLa].value;

	// Select Colors
	var xCl = getColors.selectedIndex;
	var yCl = getColors.options[xCl].value;

	outputText.style.borderColor = '#20c997';
	if ( yL === 'HTML' ) {
		convertHTML(yL, yLa, yCl, 'Hyper Text Markup Language');
	}
	if ( yL === 'SCSS' ) {
		// console.log(y);
		convertSCSS(yL, yLa, yCl, 'Sassy Cascading Style Sheets');
	}
	if ( yL === 'CSS' ) {
		// console.log(y);
		convertCSS(yL, yLa, yCl, 'Cascading Style Sheets');
	}
	outputText.select();
	outputText.focus();
}
actionConvert();

// SUBMIT BUTTON
subMit.onclick = function(){
	actionConvert();
}

// Change lable language
getL.onchange = function() {
	actionConvert();
}

// Change lable on or off
getLable.onchange = function() {
	actionConvert();
}

// Change themes
getColors.onchange = function() {
	actionConvert();
}
	



